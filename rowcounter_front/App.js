import React, { useEffect } from 'react';

import SplashScreen from 'react-native-splash-screen';
import * as pushNotifications from './src/notifications/pushNotifications';

import MainScreen from './src/screens/MainScreen';


const App = () => {

    useEffect(() => {
        SplashScreen.hide();
        pushNotifications.configure();
        pushNotifications.requestPermissions();
    });

    return (
        <MainScreen />
    );
}

export default App;
