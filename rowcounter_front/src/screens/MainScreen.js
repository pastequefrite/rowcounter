import React, { useState, useEffect } from 'react';
import { SafeAreaView, StyleSheet, View, Text, StatusBar, TouchableOpacity, Button } from 'react-native';

import { Stopwatch } from 'react-native-stopwatch-timer';
import useAppState from 'react-native-appstate-hook';

import i18n from '../i18n/i18n';

import { storeData, retrieveData } from '../localStorage/localStorage';
import Header from '../components/Header';
import Buttons from '../components/Buttons';


const MainScreen = props => {
    let currentTimer = 0;

    const { appState } = useAppState({
        onBackground: () => saveData()
    });

    const [rowCount, setRowCount] = useState(0);
    const [timer, setTimer] = useState(0);
    const [start, setStart] = useState(false);
    const [reset, setReset] = useState(false);

    useEffect(() => {
        retrieveData('currentRow').then((data) => {
            if (data !== undefined) {
                setRowCount(data);
            }
        });
        retrieveData('currentTimer').then((data) => {
            if (data !== undefined) {
                currentTimer = data;
                setTimer(currentTimer);
                setReset(true);
            }
        });
    }, [])

    const saveData = async () => {
        setStart(false);
        await storeData('currentTimer', currentTimer);
        await storeData('currentRow', rowCount);
    }

    const _actionOnRow = async (action) => {
        let value = 0;
        if (action === '+') {
            value = rowCount + 1;
        } else if (action === '-' && rowCount > 0) {
            value = rowCount - 1;
        }

        setRowCount(value);
    };

    const _timer = (action) => {
        switch (action) {
            case 'play/pause':
                setStart(!start);
                setReset(false);
                break;
            case 'reset':
                currentTimer = 0;
                setTimer(currentTimer)
                setStart(false);
                setReset(true);
                break;
            default:
                break;
        }
    };
    return (
        <SafeAreaView style={styles.mainContainer}>
            <StatusBar barStyle="light-content" />
            <View style={styles.topContainer}>
                <Header title={i18n.t('home.title')} />

                <TouchableOpacity style={styles.timer} onPress={() => _timer('play/pause')}
                    onLongPress={() => _timer('reset')}>
                    <Stopwatch options={stopWatchOptions} getMsecs={(ms) => currentTimer = ms}
                        startTime={timer} start={start} reset={reset} />
                </TouchableOpacity>
            </View>
            <View style={styles.midContainer}>
                <Text style={styles.currentRow}>{rowCount}</Text>
            </View>
            <View style={styles.botContainer}>
                <Buttons onPress={_actionOnRow} disable={rowCount === 0} />
            </View>
        </SafeAreaView>
    );
}

const stopWatchOptions = {
    container: {
        backgroundColor: '#c44569',
    },
    text: {
        color: 'white',
        fontSize: 20,
    },
};

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: '#c44569',
    },
    topContainer: {
        flex: 1,
    },
    midContainer: {
        flex: 1,
    },
    botContainer: {
        flex: 1,
        justifyContent: 'flex-end',
    },
    oneButton: {
        margin: 10,
    },
    twoButtons: {
        flexDirection: 'row',
    },
    button: {
        // margin: 10,
    },
    title: {
        color: 'white',
        fontSize: 35,
        textAlign: 'center',
        alignSelf: 'center',
    },
    currentRow: {
        color: 'white',
        fontSize: 70,
        textAlign: 'center',
        alignSelf: 'center',
    },
    timer: {
        alignSelf: 'center',
    },
});

export default MainScreen;
