import i18n from 'i18n-js';
import * as RNLocalize from "react-native-localize";

import en from './locales/en';
import fr from './locales/fr';
import de from './locales/de';
import it from './locales/it';
import es from './locales/es';


const currentLocales = RNLocalize.getLocales();

i18n.defaultLocale = 'en';
i18n.locale = currentLocales[0].languageCode;
i18n.fallbacks = true; // permet de completer par la défaultLocale si un mot n'est pas présent.
i18n.translations = { en, fr, de, it, es };


export default i18n;
