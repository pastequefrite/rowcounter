import AsyncStorage from '@react-native-community/async-storage';

export const storeData = async (key, value) => {
    try {
        await AsyncStorage.setItem('@' + key, JSON.stringify(value));
    } catch (error) {
        return error;
    }
};

export const retrieveData = async (key) => {
    try {
        const value = await AsyncStorage.getItem('@' + key);
        if (value !== null) {
            return parseInt(value);
        }
    } catch (error) {
        return error;
    }
};

