import PushNotification from 'react-native-push-notification';
import PushNotificationIOS from '@react-native-community/push-notification-ios';

import i18n from '../../src/i18n/i18n';


export const configure = () => {
    PushNotification.configure({
        // (optional) Called when Token is generated (iOS and Android)
        onRegister: function (token) {
            //process token
            console.log(token);
        },

        // (required) Called when a remote or local notification is opened or received
        onNotification: function (notification) {
            // process the notification

            console.log(notification);
            console.log('local notif open or received');
            // required on iOS only
            notification.finish(PushNotificationIOS.FetchResult.NoData);
        },

        // IOS ONLY (optional): default: all - Permissions to register.
        permissions: {
            alert: true,
            badge: true,
            sound: true,
        },

        // Should the initial notification be popped automatically
        // default: true
        popInitialNotification: true,

        /**
         * (optional) default: true
         * - Specified if permissions (ios) and token (android and ios) will requested or not,
         * - if not, you must call PushNotificationsHandler.requestPermissions() later
         */
        requestPermissions: false,
    });
};

export const localNotificationShedulded = () => {
    PushNotification.localNotificationSchedule({
        /* iOS only properties */
        //alertAction: // (optional) default: view
        //category: // (optional) default: null
        //userInfo: // (optional) default: null (object containing additional notification data)

        /* iOS and Android properties */
        title: i18n.t('notif.title'), // (optional)
        message: i18n.t('notif.message'), // (required)
        repeatType: 'week', // (optional) Repeating interval. Check 'Repeating Notifications' section for more info.

        date: new Date(Date.now() + (1 * 24*3600*1000)), // 1 day here
    });
};


export const cancelAllNotifications = () => {
    PushNotification.cancelAllLocalNotifications();
};

export const requestPermissions = () => {
    PushNotification.requestPermissions();
};
