import React from 'react';
import {StyleSheet, View} from 'react-native';

import {faUndo, faPlusCircle, faMinusCircle} from '@fortawesome/free-solid-svg-icons';
import AwesomeButton from 'react-native-really-awesome-button/src/themes/cartman';

import i18n from '../i18n/i18n';


import ButtonContent from './ButtonContent';


class Buttons extends React.Component {
    render() {
        return (
            <View>
                <View style={styles.oneButton}>
                    <AwesomeButton type={"secondary"} height={100} style={styles.test} stretch onPress={() => this.props.onPress('+')}>
                            <ButtonContent text={i18n.t('home.add')} icon={faPlusCircle} size={25}/>
                    </AwesomeButton>
                </View>

                <View style={styles.twoButtons}>
                    <View style={styles.button}>
                        <AwesomeButton height={75} disabled={this.props.disable} stretch onPress={() => this.props.onPress('-')}>
                            <ButtonContent text={i18n.t('home.previous')} icon={faMinusCircle}/>
                        </AwesomeButton>
                    </View>
                    <View style={styles.button}>
                        <AwesomeButton height={75} stretch onPress={this.props.onPress}>
                            <ButtonContent text={i18n.t('home.reset')} icon={faUndo}/>
                        </AwesomeButton>
                    </View>

                </View>
            </View>
        );
    }
};

const styles = StyleSheet.create({
    oneButton: {
        margin: 10,
    },
    twoButtons: {
        flexDirection: 'row',
    },
    button: {
        flex: 1,
        margin: 10,
    },
});

export default Buttons;
