import React from 'react';
import {Text, View, StyleSheet} from 'react-native';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';


class ButtonContent extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.buttonText}>{this.props.text}</Text>
                <FontAwesomeIcon style={styles.icon} icon={this.props.icon} size={this.props.size}/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },
    buttonText: {
        color: 'white',
        fontSize: 20,
        marginBottom: 10,
    },
    icon: {
        color: 'white',
    }
});

export default ButtonContent;
